## Propósito

Algoritmo bioinspirado na técnica de Simulated Annealing que busca decidir se existe uma interpretação lógica que satisfaça uma proposição booleana (3-CNF-SAT).

Simulated Annealing: https://en.wikipedia.org/wiki/Simulated_annealing

## Motivação

Por se tratar de um problema NP-Completo, é interessante a busca por algoritmos alternativos que alcancem resultados aproximados.

## Testes

Os testes são realizados via linha de comando. Utilize a flag --help para opções.

## Resultados

Os plote os resultados utilizando o arquivo 'plot.sh'
