#!/bin/bash

scenarios=( 'uf20-01.cnf' 'uf50-01.cnf' 'uf75-01.cnf' 'uf100-01.cnf' 'uf250-01.cnf' )

for s in "${scenarios[@]}"
do
	echo Starting $s
	pypy satsa.py -ti 1.0 -tf 0.0 -nit 100000 -nex 10 -f $s &
done
