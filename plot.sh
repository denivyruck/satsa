for file in ./output/*
do
	filename=$(basename "$file" ".dat")
	gnuplot -e filename=\"$filename\" plot_results.gnu
done
