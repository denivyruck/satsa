# -*- coding: utf-8 -*-

import argparse
import random
import math
import re
import timeit

from numpy import tanh, std, mean
from math import sqrt
from copy import deepcopy
from decimal import *

#Se a solução atingida no momento for melhor que a anterior => aceita solução
#Se for pior, a solução é aceita com uma probabilidade e^(-d/t)

class Expression(object):
	def __init__(self, var, flag):
		self.var = var
		self.flag = flag

	def get_value(self):
		if self.flag:
			return not self.var.value
		return self.var.value

class Var(object):
	def __init__(self):
		#Value is either true of false
		values = [True, False]
		self.value = values[random.randrange(0, len(values))]

	def negate(self):
		self.value = not self.value

class Clause(object):		
    def __init__(self, op):
        self.op = op
    
    #Op é um conjunto de expressões ligadas por ORs
    def get_value(self):
        for expr in self.op:
            if expr.get_value():
                return True
        return False

class Sat(object):
	def __init__(self, ti, tf, nit):
		self.ti = ti
		self.tf = tf
		self.tc = ti
		self.n = nit

		self.clauses, self.vars = self.parse_file(filename)
	
	def parse_file(self, filename):
		f = open(filename, 'r')
				
		data = []
		for line in f:
			if line[0] != 'c' and line[0] != '%' and line[0] != 0:
				data.append(re.findall(r'[+-]?\d+', line))
	
		for i in range(len(data)):
			for j in range(len(data[i])):
				data[i][j] = int(data[i][j])
		
		var = int(data[0][0])
		clauses = int(data[0][1])
		del data[0]

		clause_list = []
		var_list = [Var() for i in range(var)]

		for i in range(clauses):
			ops = []
			for j in range(3):
				if data[i][j] < 0:
					ops.append(Expression(var_list[abs(data[i][j])-1], True))
				else:
					ops.append(Expression(var_list[abs(data[i][j])-1], False))
			clause_list.append(Clause(ops))
		return clause_list, var_list

	def update_t(self, i):
		t = 0.5*(self.ti-self.tf)*(1.0-tanh((10.0*i/self.n)-5.0))
		t = t+self.tf
		self.tc = t

	def get_energy(self):
		energy = 0
		#Calculates the individual fitness
		for clause in self.clauses:
			if clause.get_value():
				energy += 1
		return energy
	
	def randomize(self):
		pos = random.randrange(0, len(self.vars))
		self.vars[pos].negate()
		
def normalize(fo):
	top = max(fo)
	for i in range(len(fo)):
		fo[i] = fo[i]/top
	return fo

def main():
	e = math.e

	temps = [0 for i in range(nit)]
	fo = [0 for i in range(nit)]
	exec_time = 0.0

	for i in range(n):
		start = timeit.default_timer()
		curr = Sat(ti, tf, nit)
		
		k = 0
		while curr.tc > curr.tf and k != nit:
			print(curr.tc, k)
			fo[k] += curr.get_energy()
			temps[k] += curr.tc

			#Create a new random answer
			new = deepcopy(curr)
			new.randomize()
			#print('%d: Solution: %d \t T: %.6lf' % (k, curr.get_energy(), curr.tc))

			delta = curr.get_energy()-new.get_energy()
			if delta <= 0:
				#Better solution found
				curr = new
			else:
				if curr.tc == 0:
					continue
				prob = e**(-delta/curr.tc)
				r = random.random()
				if r < prob:
					curr = new
			curr.update_t(k)
			k += 1
		stop = timeit.default_timer()
		exec_time += stop-start
	
	exec_time /= n
	for i in range(nit):
		temps[i] = temps[i]/n
		fo[i] = fo[i]/n

	sd = std(fo)

	out = open(output+'/'+filename.split('.')[0] + '.dat', 'w')
	for i in range(nit):
		out.write('%d %.6lf %.6f %.6lf\n' % (i, fo[i], temps[i], sd))
	out.write('%.6lf\n' % exec_time)
	out.close()

if __name__ == '__main__':
	parser = argparse.ArgumentParser(description='Do stuff')
	parser.add_argument('-ti', dest='ti', action='store', type=float, nargs=1, help='Initial T: float')
	parser.add_argument('-tf', dest='tf', action='store', type=float, nargs=1, help='Final T: float')
	parser.add_argument('-nit', dest='nit', action='store', type=int, nargs=1, help='Nr iterations: N')
	parser.add_argument('-nex', dest='nex', action='store', type=int, nargs=1, help='Nr executions: N')
	parser.add_argument('-input', dest='input', action='store', nargs=1, help='Input file')
	parser.add_argument('-output', dest='output', action='store', nargs=1, help='Output directory')
	args = parser.parse_args() 
	
	ti = args.ti[0]
	tf = args.tf[0]
	nit = args.nit[0]
	n = args.nex[0]
	filename = args.input[0]
	output = args.output[0]

	main()
